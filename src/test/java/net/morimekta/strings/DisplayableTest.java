package net.morimekta.strings;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofNanos;
import static java.time.Instant.ofEpochSecond;
import static net.morimekta.strings.Displayable.displayableDateTime;
import static net.morimekta.strings.Displayable.displayableDuration;
import static net.morimekta.strings.Displayable.displayableInstant;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DisplayableTest {
    @Test
    public void testDisplayable() {
        Displayable d = () -> "foo";
        assertThat(d.displayString(), is("foo"));
    }

    @Test
    public void testDisplayableDuration() {
        assertThat(displayableDuration(null), is("null"));
        assertThat(displayableDuration(ofNanos(0)), is("none"));
        assertThat(displayableDuration(ofNanos(123456)), is("123456 nano"));
        assertThat(displayableDuration(ofMillis(123)), is("0.123 sec"));
        assertThat(displayableDuration(ofNanos(123456789L)), is("0.123456789 sec"));
        assertThat(displayableDuration(ofMillis(123456)), is("2 min 3.456 sec"));
        assertThat(displayableDuration(ofNanos(123456789000L)), is("2 min 3.456789000 sec"));
        assertThat(displayableDuration(ofMillis(123456000L)), is("1 day 10 hr 17 min 36 sec"));
        assertThat(displayableDuration(ofMillis(123456000L).negated()), is("-1 day 10 hr 17 min 36 sec"));
        assertThat(displayableDuration(ofMillis(12345600000L)), is("142 days 21 hr 20 min"));
    }

    @Test
    public void testDisplayableDateTime() {
        assertThat(displayableDateTime((LocalDateTime) null), is("null"));
        assertThat(displayableDateTime((OffsetDateTime) null), is("null"));
        assertThat(displayableDateTime((ZonedDateTime) null), is("null"));
        assertThat(displayableDateTime(ofEpochSecond(1234567890).atOffset(ZoneOffset.ofHours(1)).toLocalDateTime()),
                   is("2009-02-14 00:31:30"));
        assertThat(displayableDateTime(ofEpochSecond(1234567890).atOffset(ZoneOffset.ofHours(1))),
                   is("2009-02-14 00:31:30 +01:00"));
        assertThat(displayableDateTime(ofEpochSecond(1234567890).atZone(ZoneId.of("Europe/Oslo"))),
                   is("2009-02-14 00:31:30 +01:00 Europe/Oslo"));
    }

    @Test
    public void testDisplayableInstant() {
        assertThat(displayableInstant(null), is("null"));
        assertThat(displayableInstant(ofEpochSecond(1234567890)),
                   is("2009-02-13 23:31:30 UTC"));
    }
}
