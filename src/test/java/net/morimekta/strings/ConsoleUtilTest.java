package net.morimekta.strings;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static net.morimekta.strings.ConsoleUtil.replaceNonPrintable;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ConsoleUtilTest {
    public static Stream<Arguments> testReplaceNonPrintable() {
        return Stream.of(
                arguments("foo", 'c', "foo"),
                arguments("\177߶", '.', ".߶"),
                arguments("\ud834f", '.', ".f"),
                arguments("f\ud834", '.', "f."),
                arguments("\ud8ff\udc55", '.', "."),
                arguments("\ud8ff\udb55", '.', ".."),
                arguments("\udc55f", '.', ".f"),
                arguments("f\udc55", '.', "f."),
                // this is a surrogate pair char
                arguments("𪚲", '.', "𪚲"));
    }

    @ParameterizedTest
    @MethodSource("testReplaceNonPrintable")
    public void testReplaceNonPrintable(String input, char replacement, String result) {
        assertThat(replaceNonPrintable(input, replacement),
                   is(result));
    }
}
