package net.morimekta.strings;

import net.morimekta.strings.chr.Color;
import net.morimekta.strings.chr.Unicode;
import org.junit.jupiter.api.Test;

import java.util.List;

import static net.morimekta.strings.StringUtil.center;
import static net.morimekta.strings.StringUtil.emptyToNull;
import static net.morimekta.strings.StringUtil.isNotEmpty;
import static net.morimekta.strings.StringUtil.isNullOrEmpty;
import static net.morimekta.strings.StringUtil.leftPad;
import static net.morimekta.strings.StringUtil.longestCommonPrefixPath;
import static net.morimekta.strings.StringUtil.printableWidth;
import static net.morimekta.strings.StringUtil.rightPad;
import static net.morimekta.strings.StringUtil.stripCommonPrefixPath;
import static net.morimekta.strings.StringUtil.stripNonPrintable;
import static net.morimekta.strings.StringUtil.toWhitespace;
import static net.morimekta.strings.StringUtil.wrap;
import static net.morimekta.strings.chr.Unicode.unicode;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class StringUtilTest {
    @Test
    public void testStringNull() {
        assertThat(isNullOrEmpty(null), is(true));
        assertThat(isNullOrEmpty(""), is(true));
        assertThat(isNullOrEmpty("foo"), is(false));
        assertThat(isNotEmpty(null), is(false));
        assertThat(isNotEmpty(""), is(false));
        assertThat(isNotEmpty("foo"), is(true));
        assertThat(emptyToNull(null), is(nullValue()));
        assertThat(emptyToNull(""), is(nullValue()));
        assertThat(emptyToNull("foo"), is("foo"));
    }

    @Test
    public void testPrintableWidth() {
        assertEquals(0, printableWidth(""));
        assertEquals(6, printableWidth("\033[32mAbcdef\033[0m"));
    }

    @Test
    public void testStripNonPrintable() {
        assertEquals("Abcdef", stripNonPrintable("\033[32mAbcdef\033[0m"));
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < 512; ++i) {
            if (i == '\033') {
                b.append((char) i);
            }
            b.append(unicode(i));
        }
        assertThat(stripNonPrintable(b.toString()).length(), is(449));
        assertThat(toWhitespace("\033[02m𪚲𪚲\033[00m"),   is("    "));
    }

    @Test
    public void testExpandTabs() {
        assertEquals("a   𪚲\n" +
                     "    foo", StringUtil.expandTabs("a\t𪚲\n\tfoo"));
        try {
            fail("no exception: " + StringUtil.expandTabs(null));
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("string == null"));
        }
        try {
            fail("no exception: " + StringUtil.expandTabs("foo", 0));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("tabWidth 0 < 1"));
        }
        try {
            fail("no exception: " + StringUtil.expandTabs("foo", 4, -1));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("offset -1 < 0"));
        }
    }

    @Test
    public void testClipWidth() {
        assertThat(StringUtil.clipWidth("b𪚲𪚲𪚲𪚲𪚲𪚲c", 10),
                   is("b𪚲𪚲𪚲𪚲"));
        assertThat(StringUtil.clipWidth(Color.GREEN + "b𪚲𪚲𪚲𪚲𪚲𪚲c" + Color.CLEAR, 10),
                   is(Color.GREEN + "b𪚲𪚲𪚲𪚲" + Color.CLEAR));
    }

    @Test
    public void testJust() {
        assertThat(rightPad("\033[02m𪚲𪚲\033[00m", 10),
                   is("\033[02m𪚲𪚲\033[00m      "));
        assertThat(leftPad("\033[02m𪚲𪚲\033[00m", 10),
                   is("      \033[02m𪚲𪚲\033[00m"));
        assertThat(center("\033[02m𪚲𪚲\033[00m", 10),
                   is("   \033[02m𪚲𪚲\033[00m   "));

        assertThat(leftPad("\033[02m𪚲𪚲\033[00m", 3),  is("\033[02m𪚲𪚲\033[00m"));
        assertThat(rightPad("\033[02m𪚲𪚲\033[00m", 3), is("\033[02m𪚲𪚲\033[00m"));
        assertThat(center("\033[02m𪚲𪚲\033[00m", 3),   is("\033[02m𪚲𪚲\033[00m"));
    }

    @Test
    public void testWrap() {
        assertThat(wrap("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor " +
                        "incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud " +
                        "exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure " +
                        "dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
                        "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt " +
                        "mollit anim id est laborum.", 45),
                   is("Lorem ipsum dolor sit amet, consectetur\n" +
                      "adipiscing elit, sed do eiusmod tempor\n" +
                      "incididunt ut labore et dolore magna aliqua.\n" +
                      "Ut enim ad minim veniam, quis nostrud\n" +
                      "exercitation ullamco laboris nisi ut aliquip\n" +
                      "ex ea commodo consequat. Duis aute irure\n" +
                      "dolor in reprehenderit in voluptate velit\n" +
                      "esse cillum dolore eu fugiat nulla pariatur.\n" +
                      "Excepteur sint occaecat cupidatat non\n" +
                      "proident, sunt in culpa qui officia deserunt\n" +
                      "mollit anim id est laborum."));

    }


    @Test
    public void testDiffCommonPrefix() {
        // Detect any common prefix.
        assertEquals(0, StringUtil.commonPrefix("abc", "xyz"),
                     "diff_commonPrefix: Null case.");

        assertEquals(4, StringUtil.commonPrefix("1234abcdef", "1234xyz"),
                     "diff_commonPrefix: Non-null case.");

        assertEquals(4, StringUtil.commonPrefix("1234", "1234xyz"),
                     "diff_commonPrefix: Whole case.");
    }

    @Test
    public void testDiffCommonSuffix() {
        // Detect any common suffix.
        assertEquals(0, StringUtil.commonSuffix("abc", "xyz"),
                     "diff_commonSuffix: Null case.");

        assertEquals(4, StringUtil.commonSuffix("abcdef1234", "xyz1234"),
                     "diff_commonSuffix: Non-null case.");

        assertEquals(4, StringUtil.commonSuffix("1234", "xyz1234"),
                     "diff_commonSuffix: Whole case.");
    }

    @Test
    public void testDiffCommonOverlap() {
        // Detect any suffix/prefix overlap.
        assertEquals(0, StringUtil.commonOverlap("", "abcd"),
                     "diff_commonOverlap: Null case.");

        assertEquals(3, StringUtil.commonOverlap("abc", "abcd"),
                     "diff_commonOverlap: Whole case.");

        assertEquals(0, StringUtil.commonOverlap("123456", "abcd"),
                     "diff_commonOverlap: No overlap.");

        assertEquals(3, StringUtil.commonOverlap("123456xxx", "xxxabcd"),
                     "diff_commonOverlap: Overlap.");

        // Some overly clever languages (C#) may treat ligatures as equal to their
        // component letters.  E.g. U+FB01 == 'fi'
        assertEquals(0, StringUtil.commonOverlap("fi", "\ufb01i"),
                     "diff_commonOverlap: Unicode.");
    }

    @Test
    public void testLongestCommonPrefix() {
        assertThat(longestCommonPrefixPath(List.of("/abba/anne", "/abba/annika")),
                   is("/abba/"));
        assertThat(longestCommonPrefixPath(List.of("/u2/bono", "/abba/annika")),
                   is("/"));
        assertThat(longestCommonPrefixPath(List.of("../u2/bono", "../abba/annika")),
                   is("../"));
        assertThat(longestCommonPrefixPath(List.of("foo/bar", "bar/foo")),
                   is(""));
    }

    @Test
    public void testStripCommonPrefix() {
        assertThat(stripCommonPrefixPath(List.of("/abba/anne", "/abba/annika")),
                   is(List.of("anne", "annika")));
        assertThat(stripCommonPrefixPath(List.of("/u2/bono", "/abba/annika")),
                   is(List.of("u2/bono", "abba/annika")));
        assertThat(stripCommonPrefixPath(List.of("../u2/bono", "../abba/annika")),
                   is(List.of("u2/bono", "abba/annika")));
        assertThat(stripCommonPrefixPath(List.of("foo/bar", "bar/foo")),
                   is(List.of("foo/bar", "bar/foo")));
    }

}
