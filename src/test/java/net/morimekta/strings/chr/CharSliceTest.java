/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.strings.chr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author Stein Eldar Johnsen
 * @since 16.01.16.
 */
public class CharSliceTest {
    private char[] data;
    private String string;

    @BeforeEach
    public void setUp() {
        string = "This is a -123.45 long \"test ł→Þħ úñí©øð€.\"";
        data = string.toCharArray();
    }

    @Test
    public void testFromString() {
        CharSlice slice = new CharSlice(string);
        CharSlice other = CharSlice.of(string);

        assertThat(slice.offset(), is(0));
        assertThat(slice.length(), is(string.length()));
        assertThat(slice.toString(), is(string));

        assertThat(slice, is(other));
        assertThat(slice, is(not(sameInstance(other))));
        assertThat(slice.subSequence(0, slice.len), is(slice));
        assertThat(slice.subSequence(0, slice.len), is(sameInstance(slice)));
        assertThat(slice.toString(), is(other.toString()));

        assertThat(CharSlice.of(slice), is(sameInstance(slice)));
    }

    @Test
    public void testFromChars() {
        CharSlice slice = new CharSlice(data);
        CharSlice other = new CharSlice(data);

        assertThat(slice.offset(), is(0));
        assertThat(slice.length(), is(data.length));
        assertThat(slice.toString(), is(string));

        assertThat(slice, is(other));
        assertThat(slice, is(not(sameInstance(other))));
        assertThat(slice.subSequence(0, slice.len), is(slice));
        assertThat(slice.subSequence(0, slice.len), is(sameInstance(slice)));
        assertThat(slice.toString(), is(other.toString()));

        try {
            new CharSlice(null, 1, 2);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No buffer"));
        }
        try {
            new CharSlice(data, -1, 5);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Negative offset: -1"));
        }
        try {
            new CharSlice(data, 1, -1);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Negative length: -1"));
        }
        try {
            new CharSlice(data, 10, 50);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Too large slice: 10+50 of 43"));
        }
    }

    @Test
    public void testSubSlice_From() {
        CharSlice slice = new CharSlice(data);
        CharSlice one = slice.subSlice(23);

        assertThat(one.offset(), is(23));
        assertThat(one.length(), is(20));
        assertThat(one.toString(), is("\"test ł→Þħ úñí©øð€.\""));

        CharSlice two = slice.subSlice(-20);
        assertThat(two, is(one));
        assertThat(two, is(not(sameInstance(one))));
        assertThat(two.toString(), is(one.toString()));
        assertThat(two.hashCode(), is(one.hashCode()));
        assertThat(two.compareTo(one), is(0));
        assertThat(slice.subSlice(22).compareTo(one), is(lessThan(0)));
        assertThat(slice.subSlice(24).compareTo(one), is(greaterThan(0)));

        try {
            slice.subSlice(-45);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("[-45:43] of slice length 43 is not valid."));
        }
        try {
            slice.subSlice(44);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("[44:43] of slice length 43 is not valid."));
        }
    }

    @Test
    public void testSubSlice() {
        CharSlice slice = new CharSlice(data);
        CharSlice one = slice.subSlice(10, 22);

        assertThat(one.offset(), is(10));
        assertThat(one.length(), is(12));
        assertThat(one.toString(), is("-123.45 long"));

        CharSlice two = slice.subSlice(-33, -21);
        assertThat(two, is(one));
        assertThat(two, is(not(sameInstance(one))));
        assertThat(two.toString(), is(one.toString()));
        assertThat(two.hashCode(), is(one.hashCode()));
        assertThat(two.compareTo(one), is(0));

        try {
            slice.subSlice(-45, 20);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("[-45:20] of slice length 43 is not valid."));
        }
        try {
            slice.subSlice(20, -45);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("[20:-45] of slice length 43 is not valid."));
        }
        try {
            slice.subSlice(44, 20);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("[44:20] of slice length 43 is not valid."));
        }
        try {
            slice.subSlice(20, 44);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("[20:44] of slice length 43 is not valid."));
        }

        try {
            slice.subSlice(20, 10);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("[20:10] of slice length 43 is not valid."));
        }
    }

    @Test
    public void testAt() {
        CharSlice slice = new CharSlice(data);
        assertThat(slice.at(11), is('1'));
        assertThat(slice.at(-32), is('1'));
    }

    @Test
    public void testSubstring() {
        CharSlice slice = new CharSlice(data, 11, 6);
        assertThat(slice.offset(), is(11));

        assertEquals("123", slice.subSequence(0, 3).toString());
        assertEquals("23.4", slice.subSequence(1, 5).toString());

        // substring cannot cut outside the boundaries of the original, only
        // trim down, even though it would have become a valid slice.
        try {
            slice.subSequence(-1, 2);
            fail();
        } catch (IllegalArgumentException e) {
            // pass()
        }
        try {
            slice.subSequence(0, 10);
            fail();
        } catch (IllegalArgumentException e) {
            // pass()
        }

        try {
            slice.subSequence(3, -4);
            fail();
        } catch (IllegalArgumentException e) {
            // pass()
        }
    }

    @Test
    public void testCharAt() {
        CharSlice slice = new CharSlice(data, 11, 6);

        assertEquals('1', slice.charAt(0));
        assertEquals('5', slice.charAt(5));

        try {
            slice.charAt(-1);
            fail();
        } catch (IllegalArgumentException e) {
            // pass()
        }

        try {
            slice.charAt(6);
            fail();
        } catch (IllegalArgumentException e) {
            // pass()
        }
    }

    @Test
    public void testHashCode() {
        CharSlice slice = new CharSlice(data, 11, 6);
        int hc1 = slice.hashCode();

        data[4] = 'y';
        // Hash code is NOT reading data content outside matched area.
        assertThat(slice.hashCode(), is(hc1));

        data[12] = 'c';
        // Hash code is reading data content of matched area only.
        assertThat(slice.hashCode(), is(not(hc1)));
    }

    @Test
    public void testEquals() {
        CharSlice slice = new CharSlice(data, 11, 6);
        CharSlice equal = new CharSlice(data, 11, 6);

        CharSlice diffOff = new CharSlice(data, 5, 6);
        CharSlice diffLen = new CharSlice(data, 11, 3);
        CharSlice diffFb = new CharSlice(Arrays.copyOf(data, data.length), 11, 6);

        assertThat(slice, is(equal));
        assertThat(slice, is(slice));
        assertThat(slice, is(not(new Object())));
        assertThat(slice.equals(null), is(false));
        assertThat(slice, is(diffFb));
        assertThat(slice, is(not(diffLen)));
        assertThat(slice, is(not(diffOff)));
    }

    @Test
    public void testCompareTo() {
        CharSlice a = new CharSlice(data, 11, 6);
        CharSlice a2 = new CharSlice(data, 11, 6);
        CharSlice b = new CharSlice(data, 11, 5);
        CharSlice c = new CharSlice(data, 14, 3);

        assertEquals(0, a.compareTo(a2));

        assertThat(a.compareTo(b), is(greaterThan(0)));
        assertThat(a.compareTo(c), is(greaterThan(0)));
        assertThat(b.compareTo(c), is(greaterThan(0)));

        assertThat(b.compareTo(a), is(lessThan(0)));
        assertThat(c.compareTo(a), is(lessThan(0)));
        assertThat(c.compareTo(b), is(lessThan(0)));

        assertThat(a.compareTo(CharSlice.of("123.4")), is(greaterThan(0)));
        assertThat(b.compareTo(CharSlice.of("123.4")), is(0));
        assertThat(b.compareTo(CharSlice.of("123.5")), is(lessThan(0)));
    }

    @Test
    public void testComparePosition() {
        CharSlice a = new CharSlice(data, 11, 6);
        CharSlice a2 = new CharSlice(data, 11, 6);
        CharSlice b = new CharSlice(data, 11, 5);
        CharSlice c = new CharSlice(data, 14, 3);

        assertEquals(0, a.comparePosition(a2));

        assertEquals(-1, a.comparePosition(b));
        assertEquals(-1, a.comparePosition(c));
        assertEquals(-1, b.comparePosition(c));

        assertEquals(1, b.comparePosition(a));
        assertEquals(1, c.comparePosition(a));
        assertEquals(1, c.comparePosition(b));
    }
}
