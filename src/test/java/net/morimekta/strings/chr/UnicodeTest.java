package net.morimekta.strings.chr;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static net.morimekta.strings.chr.Control.ctrl;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Test for unicode char.
 */
public class UnicodeTest {
    @Test
    public void testLength() {
        assertThat(new Unicode('優').length(), is(1));
        assertThat(new Unicode(0x2A6B2).length(), is(2));
        assertThat(new Unicode('a').length(), is(1));
        // TAB is rather 'undefined', but put in the bucket with other
        // control chars.
        assertThat(new Unicode(Char.TAB).length(), is(1));
        assertThat(Unicode.NBSP.length(), is(1));
    }

    @Test
    public void testPrintableWidth() {
        assertThat(Unicode.NBSP.printableWidth(), is(1));
        assertThat(new Unicode(0x0611).printableWidth(), is(0));
        assertThat(new Unicode(0x2A6B2).printableWidth(), is(2));
        assertThat(new Unicode('優').printableWidth(), is(2));
        assertThat(new Unicode('a').printableWidth(), is(1));
        // TAB is rather 'undefined', but put in the bucket with other
        // control chars.
        assertThat(new Unicode(Char.TAB).printableWidth(), is(0));

        // Run statistics over printableWidth to cover all characters [ 0 .. 0x10000 >
        int sum = 0;
        for (int i = 0; i < 0x200; ++i) {
            sum += new Unicode(i).printableWidth();
        }
        assertThat(sum, is(447));

        sum = 0;
        for (int i = 0x200; i < 0x1000; ++i) {
            sum += new Unicode(i).printableWidth();
        }
        assertThat(sum, is(2986));

        sum = 0;
        for (int i = 0x1000; i < 0x2000; ++i) {
            sum += new Unicode(i).printableWidth();
        }
        assertThat(sum, is(3750));

        sum = 0;
        for (int i = 0x2000; i < 0x4000; ++i) {
            sum += new Unicode(i).printableWidth();
        }
        assertThat(sum, is(12552));

        sum = 0;
        for (int i = 0x4000; i < 0x8000; ++i) {
            sum += new Unicode(i).printableWidth();
        }
        assertThat(sum, is(32704));

        sum = 0;
        for (int i = 0x8000; i < 0x10000; ++i) {
            sum += new Unicode(i).printableWidth();
        }
        assertThat(sum, is(53846));
    }

    @Test
    public void testToString() {
        assertThat(Unicode.NBSP.toString(), is("\u00A0"));
        assertThat(new Unicode(0x2A6B2).toString(), is("𪚲"));
        assertThat(new Unicode('優').toString(), is("優"));
        assertThat(new Unicode('a').toString(), is("a"));
        // TAB is rather 'undefined', but put in the bucket with other
        // control chars.
        assertThat(new Unicode(Char.TAB).toString(), is("\t"));
    }

    public static Stream<Arguments> testAsStringArgs() {
        return Stream.of(
                arguments(new Unicode(Char.NUL), "<NUL>"),
                arguments(new Unicode(Char.ABR), "<ABR>"),
                arguments(ctrl('c'), "<ABR>"),
                arguments(new Unicode(Char.EOF), "<EOF>"),
                arguments(ctrl('d'), "<EOF>"),
                arguments(new Unicode(Char.ENQ), "<ENQ>"),
                arguments(new Unicode(Char.ACK), "<ACK>"),
                arguments(new Unicode(Char.NAK), "<NAK>"),
                arguments(new Unicode(Char.BEL), "<BEL>"),
                arguments(new Unicode(Char.BS), "<BS>"),
                arguments(new Unicode(Char.VT), "<VT>"),
                arguments(new Unicode(Char.SS), "<SS>"),
                arguments(new Unicode(Char.SI), "<SI>"),
                arguments(new Unicode(Char.ESC), "<ESC>"),
                arguments(new Unicode(Char.CAN), "<CAN>"),
                arguments(ctrl('x'), "<CAN>"),
                arguments(ctrl('Q'), "<XON>"),
                arguments(ctrl('S'), "<XOFF>"),
                arguments(new Unicode(Char.FS), "<FS>"),
                arguments(new Unicode(Char.GS), "<GS>"),
                arguments(new Unicode(Char.RS), "<RS>"),
                arguments(new Unicode(Char.US), "<US>"),
                arguments(new Unicode(Char.DEL), "<DEL>"),

                arguments(new Unicode(Char.TAB), "\\t"),
                arguments(new Unicode(Char.LF), "\\n"),
                arguments(new Unicode(Char.FF), "\\f"),
                arguments(new Unicode(Char.CR), "\\r"),
                arguments(new Unicode('\"'), "\\\""),
                arguments(new Unicode('\''), "\\'"),
                arguments(new Unicode('\\'), "\\\\"),
                arguments(Unicode.NBSP, "<nbsp>"),
                arguments(Unicode.LTR_ISOLATE, "<ltr-isolate>"),
                arguments(Unicode.RTL_ISOLATE, "<rtl-isolate>"),
                arguments(Unicode.FS_ISOLATE, "<fs-isolate>"),
                arguments(Unicode.LTR_EMBED, "<ltr-embedded>"),
                arguments(Unicode.RTL_EMBED, "<rtl-embedded>"),
                arguments(Unicode.LTR_OVERRIDE, "<ltr-override>"),
                arguments(Unicode.RTL_OVERRIDE, "<rtl-override>"),

                arguments(new Unicode(16), "\\020"),
                arguments(new Unicode(0x10cd), "\\u10cd"),
                arguments(new Unicode(0x20002000), "\\ud7c8\\udc00"),

                arguments(new Unicode(0x2A6B2), "𪚲"),
                arguments(new Unicode('優'), "優"));
    }

    @ParameterizedTest
    @MethodSource("testAsStringArgs")
    public void testAsString(Char u, String expected) {
        assertThat(u.asString(), is(expected));
    }

    @Test
    public void testEquals() {
        Unicode a = new Unicode('a');
        Unicode a1 = new Unicode('a');
        Unicode b = new Unicode('b');

        assertThat(a.equals(a1), is(true));
        assertThat(a.equals(a), is(true));
        assertThat(a.equals(b), is(false));
        assertThat(a.equals(null), is(false));
        assertThat(a.equals(new Object()), is(false));
    }

    @Test
    public void testCompareTo() {
        Unicode a = new Unicode('a');
        Unicode a1 = new Unicode('a');
        Unicode b = new Unicode('b');

        assertThat(a.compareTo(a1), is(0));
        assertThat(a.compareTo(b), is(-1));
    }
}
