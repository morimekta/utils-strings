package net.morimekta.strings.chr;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Created by morimekta on 22.04.17.
 */
public class ControlTest {
    @Test
    public void testRemapping() {
        assertThat(new Control("\033OH"), is(Control.HOME));
        assertThat(new Control("\033[H"), is(Control.HOME));
    }

    @Test
    public void testAsString() {
        assertThat(Control.UP.asString(), is("<up>"));
        assertThat(Control.DOWN.asString(), is("<down>"));
        assertThat(Control.RIGHT.asString(), is("<right>"));
        assertThat(Control.LEFT.asString(), is("<left>"));
        assertThat(Control.CTRL_UP.asString(), is("<C-up>"));
        assertThat(Control.CTRL_DOWN.asString(), is("<C-down>"));
        assertThat(Control.CTRL_RIGHT.asString(), is("<C-right>"));
        assertThat(Control.CTRL_LEFT.asString(), is("<C-left>"));
        assertThat(Control.CURSOR_ERASE.asString(), is("<cursor-erase>"));
        assertThat(Control.CURSOR_SAVE.asString(), is("<cursor-save>"));
        assertThat(Control.CURSOR_RESTORE.asString(), is("<cursor-restore>"));
        assertThat(Control.TERMINAL_CLEAR.asString(), is("<clear>"));
        assertThat(Control.TERMINAL_RESET.asString(), is("<reset>"));
        assertThat(Control.TERMINAL_CONTROL.asString(), is("<terminal-control>"));
        assertThat(Control.TERMINAL_RESTORE.asString(), is("<terminal-restore>"));
        assertThat(Control.cursorUp(2).asString(), is("<cursor-2-up>"));
        assertThat(Control.cursorDown(3).asString(), is("<cursor-3-down>"));
        assertThat(Control.cursorRight(4).asString(), is("<cursor-4-right>"));
        assertThat(Control.cursorLeft(42).asString(), is("<cursor-42-left>"));
        assertThat(Control.DPAD_MID.asString(), is("<dpa-mid>"));
        assertThat(Control.INSERT.asString(), is("<insert>"));
        assertThat(Control.DELETE.asString(), is("<delete>"));
        assertThat(Control.HOME.asString(), is("<home>"));
        assertThat(Control.END.asString(), is("<end>"));
        assertThat(Control.PAGE_UP.asString(), is("<pg-up>"));
        assertThat(Control.PAGE_DOWN.asString(), is("<pg-down>"));
        assertThat(Control.ALT_D.asString(), is("<alt-d>"));
        assertThat(Control.ALT_K.asString(), is("<alt-k>"));
        assertThat(Control.ALT_U.asString(), is("<alt-u>"));
        assertThat(Control.ALT_W.asString(), is("<alt-w>"));
        assertThat(Control.F1.asString(), is("<F1>"));
        assertThat(Control.F2.asString(), is("<F2>"));
        assertThat(Control.F3.asString(), is("<F3>"));
        assertThat(Control.F4.asString(), is("<F4>"));
        assertThat(Control.F5.asString(), is("<F5>"));
        assertThat(Control.F6.asString(), is("<F6>"));
        assertThat(Control.F7.asString(), is("<F7>"));
        assertThat(Control.F8.asString(), is("<F8>"));
        assertThat(Control.F9.asString(), is("<F9>"));
        assertThat(Control.F10.asString(), is("<F10>"));
        assertThat(Control.F11.asString(), is("<F11>"));
        assertThat(Control.F12.asString(), is("<F12>"));
        assertThat(new Control("\033f").asString(), is("<alt-f>"));
        assertThat(new Control("\033C").asString(), is("<alt-shift-c>"));
        assertThat(new Control("\033[G").asString(), is("\\033[G"));
    }

    @Test
    public void testProperties() {
        assertThat(Control.F1.length(), is(3));
        assertThat(Control.F1.hashCode(), is(not(Control.CTRL_DOWN.hashCode())));
        assertThat(Control.F1.compareTo(Control.CTRL_DOWN), is(-12));
        assertThat(Control.F1.compareTo(Unicode.NBSP), is(-1));
        assertThat(Control.F1.equals(Control.F1), is(true));
        assertThat(Control.F1.equals(null), is(false));
        assertThat(Control.F1.equals(new Object()), is(false));
    }

    @Test
    public void testAlt() {
        try {
            Control.alt('O');
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not suitable for <alt> modifier: 'O'"));
        }
    }
}
