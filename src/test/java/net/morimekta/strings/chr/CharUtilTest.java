package net.morimekta.strings.chr;

import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.strings.chr.CharUtil.inputBytes;
import static net.morimekta.strings.chr.CharUtil.inputChars;
import static net.morimekta.strings.chr.CharUtil.makeBorder;
import static net.morimekta.strings.chr.CharUtil.makeNumeric;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests for console util static methods.
 */
public class CharUtilTest {
    @Test
    public void testBorders() {
        int ok = 0;
        int bad = 0;

        for (int[] a : new int[][]{
                {0, 0, 0, 1},
                {0, 0, 1, 1},
                {0, 1, 0, 1},
                {0, 1, 1, 1},
                {1, 1, 1, 1},

                {0, 0, 0, 2},
                {0, 0, 2, 2},
                {0, 2, 0, 2},
                {0, 2, 2, 2},
                {2, 2, 2, 2},

                {0, 1, 0, 2},
                {0, 0, 1, 2},
                {0, 0, 2, 1},

                {0, 1, 1, 2},
                {0, 1, 2, 1},
                {0, 2, 1, 1},
                {2, 1, 1, 1},

                {0, 2, 2, 1},
                {0, 2, 1, 2},
                {0, 1, 2, 2},
                {1, 2, 2, 2},

                {1, 1, 2, 2},
                {1, 2, 1, 2},

                // dl - single (no stumps)
                {0, 0, 3, 3},
                {0, 3, 0, 3},
                {0, 3, 3, 3},
                {3, 3, 3, 3},

                {0, 0, 1, 3},
                {0, 0, 3, 1},
                {0, 3, 1, 3},
                {0, 1, 3, 1},
                {1, 3, 1, 3},
                {3, 3, 3, 3},

                // Bad.
                {1, 1, 1, 3},
                {-1, 0, 0, 0},
                {4, 0, 0, 0},
        }) {
            for (int i = 0; i < 4; ++i) {
                try {
                    makeBorder(a[pos(i, 0)], a[pos(i, 1)], a[pos(i, 2)], a[pos(i, 3)]);
                    ++ok;
                } catch (IllegalArgumentException e) {
                    // e.printStackTrace();
                    ++bad;
                }
            }
        }

        assertThat(ok, is(132));
        assertThat(bad, is(12));
    }

    @Test
    public void testNumeric() {
        try {
            makeNumeric(0);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No circled numeric for 0"));
        }
        for (int i = 1; i <= 50; ++i) {
            assertThat(makeNumeric(i), is(instanceOf(Unicode.class)));
        }
        try {
            makeNumeric(51);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No circled numeric for 51"));
        }
    }

    int pos(int i, int off) {
        return (i + off) % 4;
    }

    @Test
    public void testInputBytes() {
        assertThat(inputBytes(127, ' ', Control.HOME, new Unicode(Char.ESC)),
                   is(bytes(127, 32, 27, 91, 49, 126, 27, 27)));
        assertThat(inputBytes(27),
                   is(bytes(27, 27)));
        assertThat(inputBytes('\033'),
                   is(bytes(27, 27)));
        assertThat(inputBytes("boo"),
                   is(bytes(98, 111, 111)));
        assertThat(inputBytes(new StringBuilder("boo")),
                   is(bytes(98, 111, 111)));
        assertThat(inputBytes((Object) "boo".getBytes(UTF_8)),
                   is(bytes(98, 111, 111)));
        assertThat(inputBytes((Object) "boo".toCharArray()),
                   is(bytes(98, 111, 111)));
        assertThat(inputBytes(new Object() {
                       @Override
                       public String toString() {
                           return "boo";
                       }
                   }),
                   is(bytes(98, 111, 111)));
    }

    @Test
    public void testInputChars() {
        assertThat(inputChars(127, ' ', Control.HOME, new Unicode(Char.ESC)),
                   is(ImmutableList.of(
                           new Unicode(Char.DEL),
                           new Unicode(' '),
                           Control.HOME,
                           new Unicode(Char.ESC))));
        assertThat(inputChars(27),
                   is(ImmutableList.of(new Unicode(27))));
        assertThat(inputChars('\033'),
                   is(ImmutableList.of(new Unicode(Char.ESC))));
        assertThat(inputChars("boo"),
                   is(ImmutableList.of(new Unicode('b'), new Unicode('o'), new Unicode('o'))));
        assertThat(inputChars(new StringBuilder("boo")),
                   is(ImmutableList.of(new Unicode('b'), new Unicode('o'), new Unicode('o'))));
        assertThat(inputChars((Object) "boo".getBytes(UTF_8)),
                   is(ImmutableList.of(new Unicode('b'), new Unicode('o'), new Unicode('o'))));
        assertThat(inputChars((Object) "boo".toCharArray()),
                   is(ImmutableList.of(new Unicode('b'), new Unicode('o'), new Unicode('o'))));
        assertThat(inputChars(new Object() {
                       @Override
                       public String toString() {
                           return "boo";
                       }
                   }),
                   is(ImmutableList.of(new Unicode('b'), new Unicode('o'), new Unicode('o'))));
    }


    byte[] bytes(int... b) {
        byte[] a = new byte[b.length];
        for (int i = 0; i < b.length; ++i) {
            a[i] = (byte) b[i];
        }
        return a;
    }
}
