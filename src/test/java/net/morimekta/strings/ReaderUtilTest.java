/*
 * Copyright (c) 2021, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.morimekta.strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.stream.Stream;

import static net.morimekta.strings.ReaderUtil.readAll;
import static net.morimekta.strings.ReaderUtil.readUntil;
import static net.morimekta.strings.ReaderUtil.skipUntil;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;

public class ReaderUtilTest {
    @Test
    public void testReadAll() throws IOException {
        assertThat(readAll(reader("")), is(""));
        assertThat(readAll(reader("foo")), is("foo"));

        Reader bad = Mockito.mock(Reader.class);
        Mockito.when(bad.read(any(char[].class)))
               .thenThrow(new IOException("foo"));
        try {
            readAll(bad);
            Assertions.fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("foo"));
        }
    }

    public static Stream<Arguments> argsReadUntil() {
        return Stream.of(arguments("", "a", "", ""),
                         arguments("ba", "a", "b", ""),
                         arguments("bar", "a", "b", "r"),
                         arguments("bar", "c", "bar", ""),
                         arguments("baar", "aa", "b", "r"),
                         arguments("baar", "ac", "baar", ""),
                         arguments("baar", "ca", "baar", ""));
    }

    @ParameterizedTest
    @MethodSource("argsReadUntil")
    public void testReadUntil(String content, String terminator, String out, String remaining) throws IOException {
        Reader reader = reader(content);
        assertThat(readUntil(reader, terminator), is(out));
        assertThat(readAll(reader), is(remaining));
    }

    public static Stream<Arguments> argsSkipUntil() {
        return Stream.of(arguments("", "a", false, ""),
                         arguments("ba", "a", true, ""),
                         arguments("bar", "a", true, "r"),
                         arguments("bar", "c", false, ""),
                         arguments("baar", "aa", true, "r"),
                         arguments("baar", "ac", false, ""),
                         arguments("baar", "ca", false, ""));
    }

    @ParameterizedTest
    @MethodSource("argsSkipUntil")
    public void testSkipUntil(String content, String terminator, boolean out, String remaining) throws IOException {
        Reader reader = reader(content);
        assertThat(skipUntil(reader, terminator), is(out));
        assertThat(readAll(reader), is(remaining));
    }

    @Test
    public void testUntilBadArg() throws IOException {
        try {
            readUntil(reader(""), "");
            Assertions.fail("no exceptions");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty terminator string"));
        }
        try {
            skipUntil(reader(""), "");
            Assertions.fail("no exceptions");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty terminator string"));
        }
    }

    private Reader reader(String content) {
        return new StringReader(content);
    }
}
