package net.morimekta.strings;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static net.morimekta.strings.EscapeUtil.javaEscape;
import static net.morimekta.strings.EscapeUtil.javaUnEscape;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class EscapeUtilTest {
    public static Stream<Arguments> testJavaEscape_String() {
        return Stream.of(
                arguments("foo", "foo"),
                arguments("\b\t\n\f\r\"'\\",
                          "\\b\\t\\n\\f\\r\\\"\\'\\\\"),
                arguments("\177\u0f8c",
                          "\\177\\u0f8c"),
                arguments("𪚲", "𪚲"));
    }

    @ParameterizedTest
    @MethodSource("testJavaEscape_String")
    public void testJavaEscape_String(String orig, String out) {
        assertThat(javaEscape(orig), is(out));
        assertThat(javaUnEscape(out), is(orig));
    }

    public static Stream<Arguments> dataDecode() {
        return Stream.of(
                arguments("ab\\b\\f\\n\\r\\t\\'\\\"\\\\\\u0020\\040\\0\\177·\007",
                          "ab\b\f\n\r\t'\"\\  \0\177·\007"),
                arguments("\uD800\uDC97", "\uD800\uDC97"),
                arguments("\\uD800" + '\uDC97', "\uD800\uDC97"),
                arguments('\uD800' + "\\uDC97", "\uD800\uDC97"),
                arguments("\\uD800\\uDC97", "\uD800\uDC97"));
    }

    @ParameterizedTest
    @MethodSource("dataDecode")
    public void testDecode(String str, String decoded) {
        assertThat(javaUnEscape(str, false), is(decoded));
        assertThat(javaUnEscape(str, true), is(decoded));
    }

    public static Stream<Arguments> dataDecode_StrictFail() {
        return Stream.of(
                arguments("\\01", "�", "Invalid escaped char: '\\01'"),
                arguments("\\1fbf", "�f", "Invalid escaped char: '\\1fb'"),
                arguments("\\12", "�", "Invalid escaped char: '\\12'"),
                arguments("\\lf", "�f", "Invalid escaped char: '\\l'"),

                arguments("\\uD800b", "�b", "Unmatched high surrogate char: '\\ud800'"),
                arguments("\\uD80", "�", "Invalid escaped unicode char: '\\uD80'"),
                arguments("\\u1-23f", "�f", "Invalid escaped unicode char: '\\u1-23'"),
                arguments('\uDC97' + "\\uD800f", "��f", "Unmatched low surrogate char: '\\udc97'"),
                arguments('\uD800' + "", "�", "Unmatched high surrogate char: '\\ud800'"),
                arguments("\\uD800", "�", "Unmatched high surrogate char: '\\ud800'"),
                arguments("\\uDC97\\uD800f", "��f", "Unmatched low surrogate char: '\\udc97'"),
                arguments("\\uD800\\uDC9", "��", "Invalid escaped unicode char: '\\uDC9'"),
                arguments("\\uD800\\177", "�\177", "Unmatched high surrogate char: '\\ud800'"),
                arguments("\\uD800\\u1-23", "��", "Invalid escaped unicode char: '\\u1-23'"),
                arguments("\\ud800\\uD812", "��", "Unmatched high surrogate char: '\\ud800'"));
    }

    @ParameterizedTest
    @MethodSource("dataDecode_StrictFail")
    public void testDecode_StrictFail(String str, String lenient, String strictMessage) {
        assertThat(javaUnEscape(str, false), equalTo(lenient));
        try {
            javaUnEscape(str, true);
            fail("no exception: " + strictMessage);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(strictMessage));
        }
    }

}
