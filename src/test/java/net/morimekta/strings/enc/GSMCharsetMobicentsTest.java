package net.morimekta.strings.enc;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mobicents.protocols.ss7.map.datacoding.GSMCharset;

import java.nio.charset.Charset;
import java.util.stream.Stream;

import static com.cloudhopper.commons.charset.GSMBitPacker.pack;
import static com.cloudhopper.commons.charset.GSMBitPacker.unpack;
import static net.morimekta.strings.enc.GSMCharset.GSM;
import static net.morimekta.strings.enc.GSMCharset.GSM_Urdu;
import static net.morimekta.strings.enc.GSMCharsetCloudHopperTest.testCloudHooperCompatibilityArgs;
import static net.morimekta.strings.internal.GSMCharsetUtil.URDU_LS;
import static net.morimekta.strings.internal.GSMCharsetUtil.URDU_SS;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class GSMCharsetMobicentsTest {
    private static final GSMCharset SUT_Default = new GSMCharset("GSM", new String[]{});
    private static final GSMCharset SUT_Urdu = new GSMCharset("GSM-ur", new String[]{},
            GSMCharset.BYTE_TO_CHAR_UrduAlphabet, GSMCharset.BYTE_TO_CHAR_UrduAlphabetExtentionTable);

    public static Stream<Arguments> testMobicentsCompatibilityArgs() {
        return Stream.concat(
                testCloudHooperCompatibilityArgs().map(a -> arguments(a.get()[0], GSM, SUT_Default)),
                Stream.of(
                        arguments("ریختہ کے تُمہیں اُستاد نہیں ہو یالبؔ", GSM_Urdu, SUT_Urdu),
                        arguments((URDU_LS + URDU_SS).replaceAll("[\0\1]", ""), GSM_Urdu, SUT_Urdu)));
    }

    @ParameterizedTest
    @MethodSource("testMobicentsCompatibilityArgs")
    public void testMobicentsCompatibility(String text, Charset charsetUS, Charset charsetMC) {
        // - we have to remove the extended controls.
        text = text.replaceAll("[\b\f]", "?");
        text = charsetUS == GSM_Urdu
                // - (urdu) mobicents prefer last of codepoints with ambiguous code-points: [*¡]
                // - (urdu) mobicents swap the parenthesis, and does something weird with 'M' and 'N'.
                // - (urdu) mobicents does not encode \u0654 correctly.
                ? text.replaceAll("[\\(\\)MN\u0654*¡]", "?")
                // - (default) mobicents does not encode 'Ç' correctly.
                : text.replaceAll("[Ç]", "?");

        byte[] encodedMC = unpack(text.getBytes(charsetMC));
        byte[] encodedUS = text.getBytes(charsetUS);
        String decodedMCUS = new String(encodedMC, charsetUS);
        String decodedUSMC = new String(pack(encodedUS), charsetMC);

        assertThat(decodedMCUS, is(text));
        assertThat(decodedUSMC, is(text));
        assertThat(encodedUS, is(encodedMC));
    }


}
