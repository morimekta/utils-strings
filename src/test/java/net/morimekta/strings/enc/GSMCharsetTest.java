package net.morimekta.strings.enc;

import net.morimekta.collect.util.Binary;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.util.Set;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.US_ASCII;
import static net.morimekta.strings.enc.GSMCharset.GSM;
import static net.morimekta.strings.enc.GSMCharset.GSM_Bengali;
import static net.morimekta.strings.enc.GSMCharset.GSM_Gujarati;
import static net.morimekta.strings.enc.GSMCharset.GSM_Hindi;
import static net.morimekta.strings.enc.GSMCharset.GSM_Kannada;
import static net.morimekta.strings.enc.GSMCharset.GSM_Malayalam;
import static net.morimekta.strings.enc.GSMCharset.GSM_Oriya;
import static net.morimekta.strings.enc.GSMCharset.GSM_Portuguese;
import static net.morimekta.strings.enc.GSMCharset.GSM_Punjabi;
import static net.morimekta.strings.enc.GSMCharset.GSM_Spanish;
import static net.morimekta.strings.enc.GSMCharset.GSM_Tamil;
import static net.morimekta.strings.enc.GSMCharset.GSM_Telugu;
import static net.morimekta.strings.enc.GSMCharset.GSM_Turkish;
import static net.morimekta.strings.enc.GSMCharset.GSM_Urdu;
import static net.morimekta.strings.enc.GSMCharset.UCS2;
import static net.morimekta.strings.enc.GSMCharset.detectCharset;
import static net.morimekta.strings.enc.GSMCharset.forNationalLanguageIdentifier;
import static net.morimekta.strings.enc.GSMNationalLanguageIdentifier.Bengali;
import static net.morimekta.strings.enc.GSMNationalLanguageIdentifier.Default;
import static net.morimekta.strings.enc.GSMNationalLanguageIdentifier.Gujarati;
import static net.morimekta.strings.enc.GSMNationalLanguageIdentifier.Turkish;
import static net.morimekta.strings.enc.GSMNationalLanguageIdentifier.Urdu;
import static net.morimekta.strings.enc.GSMNationalLanguageIdentifier.forCode;
import static net.morimekta.strings.enc.GSMNationalLanguageIdentifier.forIso639;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class GSMCharsetTest {
    @Test
    public void testBasicCharset() {
        assertThat(GSM.toString(), is("3GPP-23.038"));
        assertThat(GSM.aliases(), is(Set.of("GSM")));

        assertThat(GSM_Spanish.toString(), is("3GPP-23.038+es"));
        assertThat(GSM_Spanish.aliases(), is(Set.of("GSM-es")));

        assertThat(GSM_Portuguese.toString(), is("3GPP-23.038-pt+pt"));
        assertThat(GSM_Portuguese.aliases(), is(Set.of("GSM-pt")));

        assertThat(forNationalLanguageIdentifier(Default, Default), is(GSM));
        assertThat(forNationalLanguageIdentifier(Turkish, Turkish), is(GSM_Turkish));
        assertThat(forNationalLanguageIdentifier(Turkish, Urdu).toString(), is("3GPP-23.038-tr+ur"));
        assertThat(forNationalLanguageIdentifier(Turkish, Urdu).aliases(), is(Set.of()));

        assertThat(GSM.getLockingShift(), is(GSMNationalLanguageIdentifier.Default));
        assertThat(GSM.getSingleShift(), is(GSMNationalLanguageIdentifier.Default));
        assertThat(GSM.contains(GSM), is(true));
        assertThat(GSM.contains(GSM_Turkish), is(false));
        assertThat(GSM.contains(US_ASCII), is(false));

        assertThat(GSM.modifications(), is(0));
        assertThat(forNationalLanguageIdentifier(Urdu, Default).modifications(), is(1));
        assertThat(GSM_Spanish.modifications(), is(2));
        assertThat(GSM_Portuguese.modifications(), is(3));
    }

    @Test
    public void testDetectCharset() {
        assertThat(GSM.canEncode("foo"), is(true));
        assertThat(GSM.canEncode("ãnõ"), is(false));
        assertThat(detectCharset(""), is(GSM));
        assertThat(detectCharset("foo"), is(GSM));
        assertThat(detectCharset("𪚲"), is(UCS2));
        assertThat(detectCharset("∞ãnõΓ"), is(GSM_Portuguese));
        assertThat(detectCharset("İrbıl"), is(GSM_Turkish));
        assertThat(detectCharset("ਯਲ਼ਗ਼ੜ"), is(GSM_Punjabi));

        CharsetEncoder encoder = GSM_Portuguese.newEncoder();
        assertThat(encoder.canEncode('ã'), is(true));
        assertThat(encoder.canEncode('ı'), is(false));
        assertThat(encoder.canEncode("ãnõ"), is(true));
        assertThat(encoder.canEncode("İrbıl"), is(false));
    }

    public static Stream<Arguments> testDefaultEncodingArgs() {
        return Stream.of(
                arguments("foo", UCS2),
                arguments("bar{}", GSM),
                arguments("Fóç", GSM_Spanish),
                arguments("ãnõ", GSM_Portuguese),
                arguments("İrbıl", GSM_Turkish),
                arguments("চলিতভাষা", GSM_Bengali),
                arguments("પ્રસિદ્ધ દાંડી કૂચ પછી", GSM_Gujarati),
                arguments("सभी मनुष्यों कोॢ गौरव और फ़1", GSM_Hindi),
                arguments("ಎಲ್ಲಾ ಮಾನವರೂ ಸ್ವತಂತ್ರರಾಗಿಯೇ ಹುಟ್ಟಿದ್ದಾರೆ. ಹಾಗೂG", GSM_Kannada),
                arguments("ആനേ ആനകളേ മനുഷ്യാ", GSM_Malayalam),
                arguments("ଅନୁଚ୍ଛେଦ ୧: ସମସ୍ତ ମଣିଷ ଡ଼ଜନ୍ମକାଳରୁ ସ୍ୱାଧୀନ ଏବଂ", GSM_Oriya),
                arguments("ਪਰਧਾਨਲ਼ ਮੰਤਰੀ ੬੭ਗ਼ਲ਼", GSM_Punjabi),
                arguments("மனிதப் பிறிவியினர் சகலரும்", GSM_Tamil),
                arguments("తెలుగు గుణింతాలు", GSM_Telugu),
                arguments("ریختہ کے تُمہیں اُستاد نہیں ہو یالبؔ", GSM_Urdu));
    }

    @ParameterizedTest
    @MethodSource("testDefaultEncodingArgs")
    public void testDefaultEncoding(String text, Charset charset) {
        try {
            CharBuffer original = CharBuffer.wrap(text);
            ByteBuffer encoded = charset.newEncoder().encode(original);
            encoded.rewind();
            CharBuffer decoded = charset.newDecoder().decode(encoded);
            decoded.rewind();
            assertThat(decoded.toString(), is(text));
        } catch (CharacterCodingException e) {
            throw new AssertionError(e.getMessage(), e);
        }
    }

    @Test
    public void testEncoder() throws CharacterCodingException {
        CharsetEncoder encoder = GSM.newEncoder();
        ByteBuffer bb = ByteBuffer.allocate(2);
        CharBuffer cb = CharBuffer.wrap("foo");
        assertThat(encoder.encode(cb, bb, false), is(CoderResult.OVERFLOW));
        assertThat(GSM.newDecoder().decode(bb.flip()).toString(), is("fo"));

        bb = ByteBuffer.allocate(2);
        cb = CharBuffer.wrap("f{");
        assertThat(encoder.encode(cb, bb, false), is(CoderResult.OVERFLOW));
        assertThat(GSM.newDecoder().decode(bb.flip()).toString(), is("f"));

        bb = ByteBuffer.allocate(3);
        cb = CharBuffer.wrap("fõ");
        assertThat(encoder.encode(cb, bb, false), is(CoderResult.unmappableForLength(1)));
        assertThat(GSM.newDecoder().decode(bb.flip()).toString(), is("f"));

        bb = ByteBuffer.allocate(3);
        cb = CharBuffer.wrap("f\1");
        assertThat(encoder.encode(cb, bb, false), is(CoderResult.unmappableForLength(1)));
        assertThat(GSM.newDecoder().decode(bb.flip()).toString(), is("f"));
    }

    @Test
    public void testDecoder() {
        CharsetDecoder decoder = GSM.newDecoder();
        ByteBuffer bb = Binary.fromHexString("4f1b1b").getByteBuffer();
        CharBuffer cb = CharBuffer.allocate(4);
        assertThat(decoder.decode(bb, cb, true), is(CoderResult.unmappableForLength(2)));
        assertThat(cb.position(), is(1));
        assertThat(cb.flip().toString(), is("O"));

        bb = Binary.fromHexString("4f1b03").getByteBuffer();
        cb = CharBuffer.allocate(4);
        assertThat(decoder.decode(bb, cb, true), is(CoderResult.unmappableForLength(2)));
        assertThat(cb.position(), is(1));
        assertThat(cb.flip().toString(), is("O"));

        bb = Binary.fromHexString("4f1b").getByteBuffer();
        cb = CharBuffer.allocate(4);
        assertThat(decoder.decode(bb, cb, true), is(CoderResult.unmappableForLength(1)));
        assertThat(cb.position(), is(1));
        assertThat(cb.flip().toString(), is("O"));

        bb = Binary.fromHexString("4fb705").getByteBuffer();
        cb = CharBuffer.allocate(4);
        assertThat(decoder.decode(bb, cb, true), is(CoderResult.unmappableForLength(1)));
        assertThat(cb.position(), is(1));
        assertThat(cb.flip().toString(), is("O"));

        bb = Binary.fromHexString("4f1bb705").getByteBuffer();
        cb = CharBuffer.allocate(5);
        assertThat(decoder.decode(bb, cb, true), is(CoderResult.unmappableForLength(2)));
        assertThat(cb.position(), is(1));
        assertThat(cb.flip().toString(), is("O"));

        decoder = GSM_Bengali.newDecoder();
        bb = Binary.fromHexString("4f4705").getByteBuffer();
        cb = CharBuffer.allocate(4);
        assertThat(decoder.decode(bb, cb, true), is(CoderResult.unmappableForLength(1)));
        assertThat(cb.position(), is(1));
        assertThat(cb.flip().toString(), is("ঽ"));

        decoder = GSM_Punjabi.newDecoder();
        bb = Binary.fromHexString("311b2605").getByteBuffer();
        cb = CharBuffer.allocate(2);
        assertThat(decoder.decode(bb, cb, true), is(CoderResult.OVERFLOW));
        assertThat(cb.position(), is(1));
        assertThat(cb.flip().toString(), is("1"));

        bb = Binary.fromHexString("311b2605").getByteBuffer();
        cb = CharBuffer.allocate(3);
        assertThat(decoder.decode(bb, cb, true), is(CoderResult.OVERFLOW));
        assertThat(cb.position(), is(3));
        assertThat(cb.flip().toString(), is("1ਖ਼"));
    }

    @Test
    public void testNationalLanguageIdentifier() {
        assertThat(forIso639("ben"), is(Bengali));
        assertThat(forCode(5), is(Gujarati));

        try {
            forCode(55);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No language identifier for code 55"));
        }
    }

    @Test
    public void testNormalize() {
        assertThat(GSM.normalize("foo", '?'), is("foo"));
        assertThat(GSM.normalize("Fóç", '?'), is("Foc"));
        assertThat(GSM.normalize("𪚲3", '?'), is("?3"));
        assertThat(GSM.normalize("\ud8ebfoo", '?'), is("?foo"));
        assertThat(GSM.normalize("foo\ud8eb", '?'), is("foo?"));
        assertThat(GSM.normalize("\udc83foo", '?'), is("?foo"));
        assertThat(GSM.normalize("foo\udc83", '?'), is("foo?"));
        assertThat(GSM_Bengali.normalize("চলিতভাষা", '?'), is("চলিতভাষা"));
        assertThat(GSM_Bengali.normalize("চলিত𪚲çভাষা", '?'), is("চলিত?cভাষা"));
        assertThat(GSM.normalize("{ãnõ+İrbıl+Fóç}".repeat(10), '?'), is("{ano+Irb?l+Foc}".repeat(10)));

        try {
            fail("no exception: " + GSM.normalize("foo", 'ভ'));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unable to encode replacement"));
        }
    }
}
