package net.morimekta.strings.enc;

import net.morimekta.collect.util.Binary;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static net.morimekta.collect.util.Binary.encodeFromString;
import static net.morimekta.collect.util.Binary.fromHexString;
import static net.morimekta.strings.enc.TBCDCharset.TBCD;
import static net.morimekta.strings.enc.TBCDCharset.TBCD_ODD;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TBCDCharsetTest {
    @Test
    public void testSimple() {
        Binary data = encodeFromString("1234567890*#abc", TBCD);
        assertThat(data.toHexString(), is("2143658709badcfe"));
        assertThat(data.decodeToString(TBCD), is("1234567890*#abc"));

        data = encodeFromString("46123456789", TBCD);
        assertThat(data.toHexString(), is("6421436587f9"));
        assertThat(data.decodeToString(TBCD), is("46123456789"));

        data = encodeFromString("46120000030", TBCD);
        assertThat(data.toHexString(), is("6421000030f0"));
        assertThat(data.decodeToString(TBCD), is("46120000030"));

        data = encodeFromString("123456789012345", TBCD);
        assertThat(data.toHexString(), is("21436587092143f5"));
        assertThat(data.decodeToString(TBCD), is("123456789012345"));

        assertThat(TBCD.contains(StandardCharsets.UTF_8), is(false));
        assertThat(TBCD.contains(TBCD), is(true));

        data = encodeFromString("123456789012345", TBCD_ODD);
        assertThat(data.toHexString(), is("2143658709214305"));
        assertThat(data.decodeToString(TBCD_ODD), is("123456789012345"));
    }

    @Test
    public void testBadContent() {
        assertThat(fromHexString("64ff21").decodeToString(TBCD), is("46��12"));
        assertThat(fromHexString("6f4f2f").decodeToString(TBCD), is("�6�4�2"));
        assertThat(fromHexString("f6f4f1").decodeToString(TBCD), is("6�4�1"));
        assertThat(encodeFromString("123foo5", TBCD).toHexString(), is("21f3fff5"));

        assertThat(fromHexString("64ff21").decodeToString(TBCD_ODD), is("46��1"));
        assertThat(fromHexString("6f4f2f").decodeToString(TBCD_ODD), is("�6�4�"));
        assertThat(fromHexString("f6f4f1").decodeToString(TBCD_ODD), is("6�4�1"));
        assertThat(encodeFromString("123foo5", TBCD_ODD).toHexString(), is("21f3ff05"));
    }

    @Test
    public void testCanEncode() {
        assertThat(TBCD.newEncoder().canEncode('4'), is(true));
        assertThat(TBCD.newEncoder().canEncode('d'), is(false));
        assertThat(TBCD.newEncoder().canEncode("4d"), is(false));
        assertThat(TBCD.newEncoder().canEncode("12345"), is(true));
        assertThat(TBCD_ODD.newEncoder().canEncode("12345"), is(true));
    }
}
