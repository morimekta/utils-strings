package net.morimekta.strings.enc;

import net.morimekta.strings.internal.GSMCharsetUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static net.morimekta.strings.enc.GSMCharset.GSM;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class GSMCharsetCloudHopperTest {
    private static final com.cloudhopper.commons.charset.GSMCharset SUT =
            new com.cloudhopper.commons.charset.GSMCharset();

    public static Stream<Arguments> testCloudHooperCompatibilityArgs() {
        return Stream.of(
                arguments("simple text"),
                arguments("text with æøåäöñ"),
                arguments((GSMCharsetUtil.DEFAULT_BC + GSMCharsetUtil.DEFAULT_EX)
                        .replaceAll("[\0\1]", "")));
    }

    @ParameterizedTest
    @MethodSource("testCloudHooperCompatibilityArgs")
    public void testCloudHooperCompatibility(String text) {
        byte[] encodedCH = SUT.encode(text);
        byte[] encodedUS = text.getBytes(GSM);
        assertThat(encodedUS, is(encodedCH));
        String decodedCHUS = new String(encodedCH, GSM);
        String decodedUSCH = SUT.decode(encodedUS);
        assertThat(decodedCHUS, is(text));
        assertThat(decodedUSCH, is(text));
    }
}
