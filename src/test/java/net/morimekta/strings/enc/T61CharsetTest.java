package net.morimekta.strings.enc;

import static java.nio.charset.StandardCharsets.US_ASCII;
import static net.morimekta.collect.util.Binary.encodeFromString;
import static net.morimekta.strings.enc.T61Charset.T61;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import net.morimekta.collect.util.Binary;
import org.junit.jupiter.api.Test;

public class T61CharsetTest {
    @Test
    public void testSimple() {
        Binary bin = encodeFromString("æøå", T61);
        assertThat(bin.toHexString(), is("f1f9ca61"));
        assertThat(bin.decodeToString(T61), is("æøå"));

        bin = encodeFromString("foo\n-bar", T61);
        assertThat(bin.decodeToString(US_ASCII), is("foo\n-bar"));
        assertThat(T61.contains(US_ASCII), is(false));
        assertThat(T61.contains(T61), is(true));

        CharsetEncoder encoder = T61.newEncoder();
        assertThat(encoder.canEncode('c'), is(true));
        assertThat(encoder.canEncode("𪚲"), is(false));

        ByteBuffer tooSmall = ByteBuffer.allocate(2);
        assertThat(encoder.encode(CharBuffer.wrap("foo"), tooSmall, false), is(CoderResult.OVERFLOW));
        tooSmall = ByteBuffer.allocate(2);
        assertThat(encoder.encode(CharBuffer.wrap("𪚲"), tooSmall, false), is(CoderResult.unmappableForLength(2)));
        assertThat(encoder.encode(CharBuffer.wrap("π"), tooSmall, false), is(CoderResult.unmappableForLength(1)));
        assertThat(encoder.encode(CharBuffer.wrap("\udc50"), tooSmall, false), is(CoderResult.unmappableForLength(1)));
        assertThat(encoder.encode(CharBuffer.wrap("\ud850"), tooSmall, false), is(CoderResult.unmappableForLength(1)));
        assertThat(encoder.encode(CharBuffer.wrap("\ud850\udc50"), tooSmall, false), is(CoderResult.unmappableForLength(2)));
        assertThat(encoder.encode(CharBuffer.wrap("\ud850b"), tooSmall, false), is(CoderResult.unmappableForLength(1)));


        CharsetDecoder decoder = T61.newDecoder();
        ByteBuffer bb = Binary.fromHexString("3b203b").getByteBuffer();
        CharBuffer cb = CharBuffer.allocate(2);
        assertThat(decoder.decode(bb, cb, false), is(CoderResult.OVERFLOW));
        assertThat(cb.flip().toString(), is("; "));

        bb = Binary.fromHexString("3b033b").getByteBuffer();
        cb = CharBuffer.allocate(2);
        assertThat(decoder.decode(bb, cb, false), is(CoderResult.unmappableForLength(1)));
        assertThat(cb.flip().toString(), is(";"));

        bb = Binary.fromHexString("3bc3").getByteBuffer();
        cb = CharBuffer.allocate(2);
        assertThat(decoder.decode(bb, cb, false), is(CoderResult.unmappableForLength(1)));
        assertThat(cb.flip().toString(), is(";"));
    }
}
