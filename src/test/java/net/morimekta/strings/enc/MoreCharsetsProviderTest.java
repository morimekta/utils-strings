package net.morimekta.strings.enc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.jupiter.api.Assertions.fail;

import java.nio.charset.Charset;
import org.junit.jupiter.api.Test;

public class MoreCharsetsProviderTest {
    @Test
    public void testMoreCharsets() {
        assertThat(Charset.forName("TBCD"), is(TBCDCharset.TBCD));
        assertThat(Charset.forName("TBCD-odd"), is(TBCDCharset.TBCD_ODD));
        assertThat(Charset.forName("T.61"), is(T61Charset.T61));
        assertThat(Charset.forName("UCS2"), is(GSMCharset.UCS2));
        assertThat(Charset.forName("GSM"), is(GSMCharset.GSM));
        assertThat(Charset.forName("3GPP-23.038"), is(GSMCharset.GSM));
        assertThat(Charset.forName("GSM-tr"), is(GSMCharset.GSM_Turkish));
        assertThat(Charset.forName("GSM-es"), is(GSMCharset.GSM_Spanish));
        assertThat(Charset.forName("GSM-pt"), is(GSMCharset.GSM_Portuguese));
        assertThat(Charset.forName("GSM-bn"), is(GSMCharset.GSM_Bengali));
        assertThat(Charset.forName("GSM-gu"), is(GSMCharset.GSM_Gujarati));
        assertThat(Charset.forName("GSM-hi"), is(GSMCharset.GSM_Hindi));
        assertThat(Charset.forName("GSM-kn"), is(GSMCharset.GSM_Kannada));
        assertThat(Charset.forName("GSM-ml"), is(GSMCharset.GSM_Malayalam));
        assertThat(Charset.forName("GSM-or"), is(GSMCharset.GSM_Oriya));
        assertThat(Charset.forName("GSM-pa"), is(GSMCharset.GSM_Punjabi));
        assertThat(Charset.forName("GSM-ta"), is(GSMCharset.GSM_Tamil));
        assertThat(Charset.forName("GSM-te"), is(GSMCharset.GSM_Telugu));
        assertThat(Charset.forName("GSM-ur"), is(GSMCharset.GSM_Urdu));

        assertThat(Charset.forName("3GPP-23.038-tr+es").toString(),
            is("3GPP-23.038-tr+es"));
        assertThat(Charset.forName("3GPP-23.038+ur").toString(),
            is("3GPP-23.038+ur"));

        try {
            Charset.forName("3GPP-23.038-");
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("3GPP-23.038-"));
        }
        try {
            Charset.forName("3GPP-23.038-foo");
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No language id for foo for 3GPP-23.038"));
        }
    }

    @Test
    public void testAvailableCharsets() {
        assertThat(Charset.availableCharsets(),
            hasEntry(is("3GPP-23.038-tr+tr"), is(GSMCharset.GSM_Turkish)));
    }
}
