package net.morimekta.strings;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.time.ZoneId;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Stream;

import static java.time.Instant.ofEpochSecond;
import static java.time.ZoneOffset.ofHoursMinutes;
import static net.morimekta.strings.Stringable.asString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class StringableTest {
    private static class IsStringable implements Stringable {
        private final String as;

        private IsStringable(String as) {this.as = as;}

        @Override
        public String toString() {
            return "IsStringable{" +
                   "as='" + as + '\'' +
                   '}';
        }

        @Override
        public String asString() {
            return as;
        }
    }

    public static Stream<Arguments> testAsString() {
        return Stream.of(
                // null
                arguments("null", null, "null"),
                // floats
                arguments("float-01", 1, "1"),
                arguments("float-02", 1L, "1"),
                arguments("float-03", 1.0f, "1"),
                arguments("float-04", 1.1f, "1.1"),
                arguments("float-05", 1.77f, "1.77"),
                arguments("float-06", 0.42f, "0.42"),
                arguments("float-07", -1, "-1"),
                arguments("float-08", -1L, "-1"),
                arguments("float-09", -1.0f, "-1"),
                arguments("float-10", -1.1f, "-1.1"),
                arguments("float-11", -1.77f, "-1.77"),
                arguments("float-12", -0.42f, "-0.42"),

                arguments("float-pre-01", .00004242424242f, "4.242424E-5"),
                arguments("float-pre-02", .0004242424242f, "0.0004242424"),
                arguments("float-pre-03", .004242424242f, "0.004242424"),
                arguments("float-pre-04", .04242424242f, "0.04242424"),
                arguments("float-pre-05", .4242424242f, "0.4242424"),
                arguments("float-pre-06", 4.2424242f, "4.242424"),
                arguments("float-pre-07", 42.424242f, "42.42424"),
                arguments("float-pre-08", 424.24242f, "424.2424"),
                arguments("float-pre-09", 4242.4242f, "4242.424"),
                arguments("float-pre-10", 42424.242f, "42424.24"),
                arguments("float-pre-11", 424242.42f, "424242.4"),
                arguments("float-pre-12", 4242424.2f, "4242424"),
                arguments("float-pre-13", 42424242.f, "4.242424E7"),
                arguments("float-pre-14", -.00004242424242f, "-4.242424E-5"),
                arguments("float-pre-15", -.0004242424242f, "-0.0004242424"),
                arguments("float-pre-16", -.004242424242f, "-0.004242424"),
                arguments("float-pre-17", -.04242424242f, "-0.04242424"),
                arguments("float-pre-18", -.4242424242f, "-0.4242424"),
                arguments("float-pre-19", -4.2424242f, "-4.242424"),
                arguments("float-pre-20", -42.424242f, "-42.42424"),
                arguments("float-pre-21", -424.24242f, "-424.2424"),
                arguments("float-pre-22", -4242.4242f, "-4242.424"),
                arguments("float-pre-23", -42424.242f, "-42424.24"),
                arguments("float-pre-24", -424242.42f, "-424242.4"),
                arguments("float-pre-56", -4242424.2f, "-4242424"),
                arguments("float-pre-26", -42424242.f, "-4.242424E7"),

                arguments("float-pre-27", .00000042f, "4.2E-7"),
                arguments("float-pre-28", 42.424242e30f, "4.242424E31"),

                // doubles
                arguments("double-01", 1.0, "1"),
                arguments("double-02", 0.42, "0.42"),
                arguments("double-03", 1.1, "1.1"),
                arguments("double-04", 4.2, "4.2"),
                arguments("double-05", 42.2, "42.2"),
                arguments("double-06", 420.2, "420.2"),
                arguments("double-07", 4200.2, "4200.2"),
                arguments("double-08", 42000.2, "42000.2"),
                arguments("double-09", 42000000000000.2,  "42000000000000.2"),
                arguments("double-10", 420000000000000.2, "420000000000000.2"),
                arguments("double-11", 420000000000000.,  "420000000000000"),
                arguments("double-12", 4200000000000000.2,  "4.2E15"),
                arguments("double-13", 4200000000000000.,   "4.2E15"),
                arguments("double-14", 42000000000000000.2, "4.2E16"),
                arguments("double-15", .42, "0.42"),
                arguments("double-16", .200000000000042,   "0.200000000000042"),
                arguments("double-17", .2000000000000042,  "0.2000000000000042"),
                arguments("double-18", .20000000000000042, "0.2000000000000004"),
                arguments("double-19", .200000000000000042, "0.2"),
                arguments("double-20", .020000000000004242, "0.02000000000000424"),
                arguments("double-21", .0020000000000042424, "0.002000000000004242"),
                arguments("double-22", .00020000000000042424, "0.0002000000000004242"),
                arguments("double-23", .000020000000000042424, "0.00002000000000004242"),
                arguments("double-24", .0000020000000000042424, "0.000002000000000004242"),
                arguments("double-25", .00000020000000000042424, "0.0000002000000000004242"),
                arguments("double-26", .000000020000000000042424, "0.00000002000000000004242"),
                arguments("double-27", .0000000020000000000042424, "2.000000000004242E-9"),
                arguments("double-32", 42e30, "4.2E31"),

                // time
                arguments("duration", Duration.ofMillis(123456),
                          "Duration{2 min 3.456 sec}"),
                arguments("duration", Duration.ofMillis(0),
                          "Duration{none}"),
                arguments("LocalDateTime", ofEpochSecond(1234567890).atZone(ZoneId.of("Europe/Oslo")).toLocalDateTime(),
                          "LocalDateTime{2009-02-14 00:31:30}"),
                arguments("OffsetDateTime", ofEpochSecond(1234567890).atOffset(ofHoursMinutes(-7, -30)),
                          "OffsetDateTime{2009-02-13 16:01:30 -07:30}"),
                arguments("ZonedDateTime", ofEpochSecond(1234567890).atZone(ZoneId.of("Europe/Oslo")),
                          "ZonedDateTime{2009-02-14 00:31:30 +01:00 Europe/Oslo}"),
                arguments("Instant", ofEpochSecond(1234567890),
                          "Instant{2009-02-13 23:31:30 UTC}"),

                // stringable
                arguments("stringable", new IsStringable("foo"), "foo"),
                // collections
                arguments("list", List.of("foo", "bar"), "[\"foo\",\"bar\"]"),
                arguments("set", new TreeSet<>(Set.of("foo", "bar")),
                          "{\"bar\",\"foo\"}"),
                arguments("map", new TreeMap<>(Map.of(42, "bar", 127, "esc")),
                          "{42:\"bar\",127:\"esc\"}"));
    }

    @MethodSource("testAsString")
    @ParameterizedTest(name = "[{index}]: {0}")
    public void testAsString(String name, Object value, String expected) {
        assertThat(asString(value), is(expected));
    }

    @Test
    public void testAsStringTypedNull() {
        assertThat(asString((Collection<?>) null), is("null"));
        assertThat(asString((Map<?, ?>) null), is("null"));
    }
}
