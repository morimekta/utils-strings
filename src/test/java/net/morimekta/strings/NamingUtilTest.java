package net.morimekta.strings;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static net.morimekta.strings.NamingUtil.Format.CAMEL;
import static net.morimekta.strings.NamingUtil.Format.LISP;
import static net.morimekta.strings.NamingUtil.Format.NAMESPACE;
import static net.morimekta.strings.NamingUtil.Format.PASCAL;
import static net.morimekta.strings.NamingUtil.Format.SNAKE;
import static net.morimekta.strings.NamingUtil.Format.SNAKE_UPPER;
import static net.morimekta.strings.NamingUtil.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class NamingUtilTest {
    @Test
    public void testCompleteness() {
        for (NamingUtil.Format format : NamingUtil.Format.values()) {
            assertThat(format("foo-bar-baz", format), is(notNullValue()));
        }
    }

    public static Stream<Arguments> testFormat() {
        return Stream.of(
                // NAMESPACE
                arguments("foo", NAMESPACE, "foo"),
                arguments("fooBar", NAMESPACE, "foo.bar"),
                arguments("foo5Bar", NAMESPACE, "foo5.bar"),
                arguments("foo5bar", NAMESPACE, "foo5bar"),
                arguments("foo_bar", NAMESPACE, "foo.bar"),
                arguments("foo-bar", NAMESPACE, "foo.bar"),
                arguments("FOO_BAR", NAMESPACE, "foo.bar"),
                arguments("FOO_5BAR", NAMESPACE, "foo.5bar"),
                // LIST
                arguments("foo", LISP, "foo"),
                arguments("fooBar", LISP, "foo-bar"),
                arguments("foo5Bar", LISP, "foo5-bar"),
                arguments("foo5bar", LISP, "foo5bar"),
                arguments("foo_bar", LISP, "foo-bar"),
                arguments("foo-bar", LISP, "foo-bar"),
                arguments("FOO_BAR", LISP, "foo-bar"),
                arguments("FOO_5BAR", LISP, "foo-5bar"),
                // SNAKE
                arguments("foo", SNAKE, "foo"),
                arguments("fooBar", SNAKE, "foo_bar"),
                arguments("foo5Bar", SNAKE, "foo5_bar"),
                arguments("foo_bar", SNAKE, "foo_bar"),
                arguments("foo-bar", SNAKE, "foo_bar"),
                arguments("FOO_BAR", SNAKE, "foo_bar"),
                arguments("FOO_5BAR", SNAKE, "foo_5bar"),
                // SNAKE_UPPER
                arguments("foo", SNAKE_UPPER, "FOO"),
                arguments("fooBar", SNAKE_UPPER, "FOO_BAR"),
                arguments("foo5Bar", SNAKE_UPPER, "FOO5_BAR"),
                arguments("foo_bar", SNAKE_UPPER, "FOO_BAR"),
                arguments("foo-bar", SNAKE_UPPER, "FOO_BAR"),
                arguments("FOO_BAR", SNAKE_UPPER, "FOO_BAR"),
                arguments("FOO_5BAR", SNAKE_UPPER, "FOO_5BAR"),
                // CAMEL
                arguments("foo", PASCAL, "Foo"),
                arguments("fooBar", PASCAL, "FooBar"),
                arguments("foo5Bar", PASCAL, "Foo5Bar"),
                arguments("foo_bar", PASCAL, "FooBar"),
                arguments("foo-bar", PASCAL, "FooBar"),
                arguments("FOO_BAR", PASCAL, "FooBar"),
                // yes, this does not make a consistent name, but it looks OK.
                arguments("FOO_5BAR", PASCAL, "Foo5bar"),
                // DROMEDARY
                arguments("foo", CAMEL, "foo"),
                arguments("fooBar", CAMEL, "fooBar"),
                arguments("foo5Bar", CAMEL, "foo5Bar"),
                arguments("foo_bar", CAMEL, "fooBar"),
                arguments("foo-bar", CAMEL, "fooBar"),
                arguments("FOO_BAR", CAMEL, "fooBar"),
                // yes, this does not make a consistent name, but it looks OK.
                arguments("FOO_5BAR", CAMEL, "foo5bar"));
    }

    @ParameterizedTest
    @MethodSource("testFormat")
    public void testFormat(String input, NamingUtil.Format format, String output) {
        assertThat(format(input, format), is(output));
    }

    @Test
    public void testFormatExhaustive() {
        for (NamingUtil.Format format : NamingUtil.Format.values()) {
            assertThat(format("foo_bar", format), is(notNullValue()));
        }
    }
}
