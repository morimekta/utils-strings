package net.morimekta.strings.diff;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class PatchUtilTest {
    @Test
    public void testSplitLines() {

    }

    public static Stream<Arguments> testPatch() {
        return Stream.of(
                arguments("diff-01"),
                arguments("diff-02"),
                arguments("diff-03"),
                arguments("diff-04"),
                arguments("diff-05"),
                arguments("diff-06"));
    }

    @ParameterizedTest
    @MethodSource("testPatch")
    public void testPatch(String name) {
        String source = readResource(name, "source");
        String target = readResource(name, "target");
        String expectedPatch = readResource(name, "patch");

        List<Change> changeList = PatchUtil.diffLines(source, target);
        String realPatch = PatchUtil.makePatchString(changeList);

        assertThat(realPatch, is(expectedPatch));
    }

    public static Stream<Arguments> testDisplayPatchParams() {
        return Stream.of(arguments("display-patch-01", PatchOptions.newBuilder()
                                                                   .withDefaultMinimal()
                                                                   .withAfter(1)
                                                                   .withBefore(1)
                                                                   .build()),
                         arguments("display-patch-02", PatchOptions.newBuilder()
                                                                   .withDefaultPretty()
                                                                   .build()),
                         arguments("display-patch-03", PatchOptions.newBuilder()
                                                                   .withDefaultMinimal()
                                                                   .build()),
                         arguments("display-patch-04", PatchOptions.newBuilder()
                                                                   .withAfter(1)
                                                                   .withBefore(1)
                                                                   .build()));
    }

    @ParameterizedTest
    @MethodSource("testDisplayPatchParams")
    public void testDisplayPatch(String name, PatchOptions options) {
        String source = readResource(name, "source");
        String target = readResource(name, "target");

        List<Change> changes = PatchUtil.diffLines(source, target);

        String expectedPatch = readResource(name, "patch");
        String actualPatch = PatchUtil.makeDisplayPatchString(changes, options);

        assertThat(actualPatch, is(expectedPatch));
    }

    static String readResource(String name, String variant) {
        String path = "/net/morimekta/strings/diff/" + name + "." + variant + ".txt";
        try (InputStream in = PatchUtilTest.class.getResourceAsStream(path);
             BufferedInputStream bin = new BufferedInputStream(in)) {
            return new String(bin.readAllBytes(), UTF_8);
        } catch (IOException e) {
            throw new AssertionError("No such resource: " + path);
        }
    }
}
