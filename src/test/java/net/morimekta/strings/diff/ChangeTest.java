package net.morimekta.strings.diff;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class ChangeTest {
    @Test
    public void testChange() {
        Change change1 = new Change(Operation.EQUAL, "foo");
        Change change1b = new Change(Operation.EQUAL, "foo");
        Change change2 = new Change(Operation.INSERT, "bar");

        assertThat(change1, is(change1));
        assertThat(change1, is(change1b));
        assertThat(change1, is(not(change2)));
        assertThat(change1, is(not("foo")));

        assertThat(change1.hashCode(), is(change1.hashCode()));
        assertThat(change1.hashCode(), is(change1b.hashCode()));
        assertThat(change1.hashCode(), is(not(change2.hashCode())));

        assertThat(change1.toString(), is("Change(EQUAL,\"foo\")"));
        assertThat(change2.toString(), is("Change(INSERT,\"bar\")"));
    }
}
