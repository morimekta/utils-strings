package net.morimekta.strings.diff;

import net.morimekta.strings.chr.CharSlice;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class DiffStringUtilTest {
    @Test
    public void testSplitLines() {
        // no ending newline.
        String str = "foo bar";
        CharSlice       slice = new CharSlice(str.toCharArray());
        List<CharSlice> split = DiffStringUtil.splitLines(slice);
        assertThat(split, contains(slice));

        // MSFT newlines.
        str = "foo\r\nbar\r\n";
        slice = new CharSlice(str.toCharArray());
        split = DiffStringUtil.splitLines(slice);
        assertThat(split, contains(slice.subSlice(0, 3), slice.subSlice(5, 8)));

        // MacOS 9 newlines.
        str = "foo\rbar\r";
        slice = new CharSlice(str.toCharArray());
        split = DiffStringUtil.splitLines(slice);
        assertThat(split, contains(slice.subSlice(0, 3), slice.subSlice(4, 7)));

        // *NIX newlines.
        str = "foo\nbar\n";
        slice = new CharSlice(str.toCharArray());
        split = DiffStringUtil.splitLines(slice);
        assertThat(split, contains(slice.subSlice(0, 3), slice.subSlice(4, 7)));
    }
}
