import net.morimekta.strings.enc.MoreCharsetsProvider;

import java.nio.charset.spi.CharsetProvider;

/**
 * Package containing utilities for handling strings and characters
 * in various combinations.
 */
module net.morimekta.strings {
    exports net.morimekta.strings;
    exports net.morimekta.strings.chr;
    exports net.morimekta.strings.enc;
    exports net.morimekta.strings.io;

    provides CharsetProvider with MoreCharsetsProvider;
}