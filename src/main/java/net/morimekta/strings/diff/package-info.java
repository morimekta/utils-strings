/**
 * This package contains utilities for detecting differences and similarities
 * between strings.
 */
package net.morimekta.strings.diff;