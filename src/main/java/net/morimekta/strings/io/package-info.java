/**
 * Package containing IO helpers for handling unusual ways of
 * buffering (by line), and UTF-8 streams without any buffering
 * over what is strictly needed for the next code-point.
 */
package net.morimekta.strings.io;