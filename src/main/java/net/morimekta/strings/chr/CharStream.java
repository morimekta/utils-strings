/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.strings.chr;

import java.util.Iterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Java 8 streams handling of character sequences.
 */
public final class CharStream {
    /**
     * Iterate over parsed <code>Char</code> sequences in string.
     *
     * @param str String to iterate over.
     * @return Char iterator.
     */
    public static Iterator<Char> iterator(CharSequence str) {
        return Spliterators.iterator(new CharSpliterator(str, false));
    }

    /**
     * Iterate over parsed <code>Char</code> sequences in string with lenient parsing.
     *
     * @param str String to iterate over.
     * @return Char iterator.
     */
    public static Iterator<Char> lenientIterator(CharSequence str) {
        return Spliterators.iterator(new CharSpliterator(str, true));
    }

    /**
     * Stream over parsed <code>Char</code> sequences in string.
     *
     * @param str String to stream over.
     * @return Char stream.
     */
    public static Stream<Char> stream(CharSequence str) {
        return StreamSupport.stream(new CharSpliterator(str, false), false);
    }

    /**
     * Stream over parsed <code>Char</code> sequences in string with lenient parsing.
     *
     * @param str String to stream over.
     * @return Char stream.
     */
    public static Stream<Char> lenientStream(CharSequence str) {
        return StreamSupport.stream(new CharSpliterator(str, true), false);
    }

    private CharStream() {}
}
