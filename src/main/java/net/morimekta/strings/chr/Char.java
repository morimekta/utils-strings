/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.strings.chr;

import net.morimekta.strings.Stringable;

/**
 * General interface implemented by character-like classes. A char
 * can represent an actual character (Unicode), or a control sequence
 * (Control, Color) that can alter the look or behaviour of the console
 * the application runs in.
 * <p>
 * Note that:
 * <ul>
 *   <li>
 *       {@link Object#toString()} returns the string value to be printed when
 *       the char should be printed to console directly.
 *   </li>
 *   <li>
 *       {@link #asString()} returns a human understandable string describing
 *       the character.
 *   </li>
 *   <li>
 *       {@link #codepoint()} returns the unicode char codepoint, and if the
 *       char is a multi-char control sequence, the codepoint of the first
 *       char in the sequence. Note that {@link Char} is <b>NOT</b> a char
 *       sequence itself.
 *   </li>
 * </ul>
 */
public interface Char extends Comparable<Char>, Stringable {
    /** Null. */
    char NUL = '\0';
    /** Abort - [Control c] */
    char ABR = '\003';
    /** End of Transmission - [Control d] */
    char EOF = '\004';
    /** Enquire Feedback. */
    char ENQ = '\005';
    /** Acknowledge Enquiry. */
    char ACK = '\006';
    /** Negative Acknowledge. */
    char NAK = '\025';
    /** Bell. */
    char BEL = '\007';
    /** Backspace: Not used on Linux */
    char BS  = '\010';
    /** Horizontal Tab. */
    char TAB = '\t';
    /** Line Feed (newline). */
    char LF  = '\n';
    /** Vertical Tab. */
    char VT  = '\013';
    /** Switch to an alternative character set (e.g. shift out of default). */
    char SS  = '\016';
    /** Return to regular character set (a.k.a. shift in) after shift out. */
    char SI  = '\017';
    /** Output ON. */
    char XON = '\021';
    /** Output OFF. */
    char XOFF = '\023';
    /** Form Feed. */
    char FF  = '\f';
    /** Carriage Return. */
    char CR  = '\r';
    /** Cancel. */
    char CAN = '\030';
    /** Escape. */
    char ESC = '\033';

    /** File Separator. */
    char FS  = '\034';
    /** Group Separator. */
    char GS  = '\035';
    /** Record Separator. */
    char RS  = '\036';
    /** Unit Separator. */
    char US  = '\037';
    /**
     * DEL, A.K.A. "Backspace"
     *
     * Note: This is a Linux keyboard weirdness, that backspace key returns
     * DEL (\0177), not BS (\010), which is the ASCII control char made
     * specifically to represent that key...
     */
    char DEL = '\177';

    /**
     * Unicode codepoint representing this character.
     *
     * @return The 31 bit unsigned unicode code-point, or -1 if not
     *         representing a single unicode character.
     */
    int codepoint();

    /**
     * The number of character spaces taken up by this symbol. Usually 1, but
     * 0 for control sequences, control characters, and 2 for wide symbols like
     * CJK characters.
     *
     * @return The printable width of the character.
     */
    int printableWidth();

    /**
     * Number of utf-16 characters that this character takes up if enclosed
     * in a java string.
     *
     * @return The char count of the character.
     */
    int length();
}
