/**
 * The package contains classes helping with managing characters
 * and the interface between character arrays and strings, and
 * between symbolic "characters", e.g. control sequences and
 * strings.
 */
package net.morimekta.strings.chr;