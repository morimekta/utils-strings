/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.strings;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Interface for making objects displayable. This is handy when handling
 * "smart" char sequences, as a string telling what the sequence represents,
 * not just it's string content, or for exceptions that want more than just
 * a plain message but e.g. a multi-line explanation of the failure.
 */
public interface Displayable {
    /**
     * Get the displayable string for this instance.
     *
     * @return A displayable string value for the object. This is meant to
     * be printed e.g. to console output and interpreted by a human.
     * The output string may contain multiple lines, but should not
     * end with a newline.
     */
    String displayString();

    /**
     * Make a displayable string out of a duration. This string is meant to be
     * easy readable. Examples:
     * <ul>
     *   <li><code>null</code>
     *   <li><code>immediately</code>
     *   <li><code>123456 nano</code>
     *   <li><code>0.123 sec</code>
     *   <li><code>0.123456789 sec</code>
     *   <li><code>2 min 3.456 sec</code>
     *   <li><code>2 min 3.456789000 sec</code>
     *   <li><code>1 day 10 hr 17 min 36 sec</code>
     *   <li><code>-1 day 10 hr 17 min 36 sec</code>
     *   <li><code>142 days 21 hr 20 min</code>
     * </ul>
     *
     * @param duration The duration to make displayable string from.
     * @return A displayable duration string. E.g. "1 day 2 hr 3 min 4.567 sec"
     * or "null".
     */
    static String displayableDuration(Duration duration) {
        if (duration == null) {
            return "null";
        }
        if (duration.getSeconds() == 0 && duration.getNano() == 0) {
            return "none";
        }
        if (duration.isNegative()) {
            return "-" + displayableDuration(duration.abs());
        }

        StringBuilder builder = new StringBuilder();
        var days = DAYS.convert(duration);
        if (days > 0) {
            builder.append(days).append(days > 1 ? " days " : " day ");
            duration = duration.minusDays(days);
        }
        var hours = HOURS.convert(duration);
        if (hours > 0) {
            builder.append(hours).append(" hr ");
            duration = duration.minusHours(hours);
        }
        var minutes = MINUTES.convert(duration);
        if (minutes > 0) {
            builder.append(minutes).append(" min ");
            duration = duration.minusMinutes(minutes);
        }
        var seconds = SECONDS.convert(duration);
        if (seconds > 0) {
            builder.append(seconds);
            duration = duration.minusSeconds(seconds);
        }
        var millis = MILLISECONDS.convert(duration);
        duration = duration.minusMillis(millis);
        var nanos = NANOSECONDS.convert(duration);
        if (nanos > 0) {
            if (seconds == 0 && millis == 0) {
                builder.append(nanos).append(" nano");
            } else {
                if (seconds > 0) {
                    builder.append(".");
                } else {
                    builder.append("0.");
                }
                builder.append(String.format("%03d%06d sec", millis, nanos));
            }
        } else if (millis > 0) {
            if (seconds > 0) {
                builder.append(".");
            } else {
                builder.append("0.");
            }
            builder.append(String.format("%03d sec", millis));
        } else if (seconds > 0) {
            builder.append(" sec");
        }
        return builder.toString().trim();
    }

    /**
     * Get displayable string for local date time.
     *
     * @param localDateTime Date-Time object to make display string of.
     * @return The formatted local date string.
     */
    static String displayableDateTime(LocalDateTime localDateTime) {
        if (localDateTime == null) return Stringable.NULL;
        return DateTimeFormatter.ISO_LOCAL_DATE_TIME
                .format(localDateTime)
                // replace T with space.
                .replace('T', ' ');
    }

    /**
     * Get displayable string for offset date time.
     *
     * @param offsetDateTime Date-Time object to make display string of.
     * @return The formatted local date string.
     */
    static String displayableDateTime(OffsetDateTime offsetDateTime) {
        if (offsetDateTime == null) return Stringable.NULL;
        return DateTimeFormatter.ISO_OFFSET_DATE_TIME
                .format(offsetDateTime)
                // replace T with space.
                .replace('T', ' ')
                // Remove offset if none.
                .replaceAll("\\+00:00$", "")
                // Add space before offset.
                .replaceAll("([+-]\\d\\d):(\\d\\d)$", " $1:$2");
    }

    /**
     * Get displayable string for zoned date time.
     *
     * @param zonedDateTime Date-Time object to make display string of.
     * @return The formatted local date string.
     */
    static String displayableDateTime(ZonedDateTime zonedDateTime) {
        if (zonedDateTime == null) return Stringable.NULL;
        // Just use the (current) zone offset + zone name.
        return displayableDateTime(zonedDateTime.toOffsetDateTime()) + " " + zonedDateTime.getZone().toString();
    }

    /**
     * Get displayable string for instant date time.
     *
     * @param instant Instant to make display string of.
     * @return The formatted instant string.
     */
    static String displayableInstant(Instant instant) {
        if (instant == null) return Stringable.NULL;
        return DateTimeFormatter.ISO_INSTANT
                .format(instant)
                // replace T with space.
                .replace('T', ' ')
                // Add space before Z (and use UTC).
                .replaceAll("(\\d)Z$", "$1 UTC");
    }
}
