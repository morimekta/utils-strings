/**
 * Package containing utilities for handling strings and characters
 * in various combinations.
 */
package net.morimekta.strings;