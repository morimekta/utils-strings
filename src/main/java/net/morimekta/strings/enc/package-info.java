/**
 * This package contains charsets and string encoding utilities.
 */
package net.morimekta.strings.enc;