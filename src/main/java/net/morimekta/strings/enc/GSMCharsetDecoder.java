/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.strings.enc;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.util.Map;

import static net.morimekta.strings.internal.GSMCharsetUtil.EXT_CODE;
import static net.morimekta.strings.internal.GSMCharsetUtil.SHIFT_SEPTET;

class GSMCharsetDecoder extends CharsetDecoder {
    private final String basic;
    private final String shift;
    private final Map<Byte, String> basicLong;
    private final Map<Byte, String> shiftLong;

    GSMCharsetDecoder(GSMCharset cs) {
        super(cs, 1f, cs.getLockingShift().basicCodeString.isEmpty() ? 1.0f : 2.0f);
        this.basic = cs.getLockingShift().basic;
        this.basicLong = cs.getLockingShift().basicCodeString;
        this.shift = cs.getSingleShift().shift;
        this.shiftLong = cs.getSingleShift().shiftCodeString;
    }

    @Override
    protected CoderResult decodeLoop(ByteBuffer byteBuffer, CharBuffer charBuffer) {
        while (byteBuffer.hasRemaining()) {
            if (!charBuffer.hasRemaining()) {
                return CoderResult.OVERFLOW;
            }

            int l = 1;
            byte b = byteBuffer.get();
            if (b < 0) {
                byteBuffer.position(byteBuffer.position() - 1);
                return CoderResult.unmappableForLength(1);
            }

            String charset = basic;
            Map<Byte, String> charsetLong = basicLong;
            if (b == SHIFT_SEPTET) {
                if (!byteBuffer.hasRemaining()) {
                    byteBuffer.position(byteBuffer.position() - 1);
                    return CoderResult.unmappableForLength(1);
                }

                l = 2;
                charset = shift;
                charsetLong = shiftLong;
                b = byteBuffer.get();
                if (b < 0) {
                    byteBuffer.position(byteBuffer.position() - 2);
                    return CoderResult.unmappableForLength(2);
                }
            }

            char c = charset.charAt(b);
            if (c == EXT_CODE) {
                if (charBuffer.remaining() < 2) {
                    byteBuffer.position(byteBuffer.position() - l);
                    return CoderResult.OVERFLOW;
                }
                charBuffer.put(charsetLong.get(b));
            } else if (c < EXT_CODE) {
                byteBuffer.position(byteBuffer.position() - l);
                return CoderResult.unmappableForLength(l);
            } else {
                charBuffer.put(c);
            }
        }
        return CoderResult.UNDERFLOW;
    }
}
